package com.dh.chat.sales.service.model.repositories;

import com.dh.chat.sales.service.model.domain.Client;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author MarvinTola
 */
public interface ClientRepository extends JpaRepository<Client,Long> {
}
