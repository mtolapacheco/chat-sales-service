package com.dh.chat.sales.service.model.domain;

import javax.persistence.*;
import java.util.Date;

/**
 * @author MarvinTola
 */
@Entity
@Table(name = "sale_table")
@PrimaryKeyJoinColumn(name = "saleid",referencedColumnName = "personid")
public class Sale {

    @Id
    @Column(name = "saleid",nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "salary",nullable = false)
    private Long numbersale;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "createddate",nullable = false)
    private Date createddate;

    @ManyToOne(fetch = FetchType.LAZY,optional = false)
    @JoinColumn(name = "saleemployeeid",referencedColumnName = "employeeid",nullable = false)
    private Employee employee;

    @ManyToOne(fetch=FetchType.LAZY,optional = false)
    @JoinColumn(name = "saleclientid",referencedColumnName = "clientid",nullable = false)
    private Client client;

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getNumbersale() {
        return numbersale;
    }

    public void setNumbersale(Long numbersale) {
        this.numbersale = numbersale;
    }

    public Date getCreateddate() {
        return createddate;
    }

    public void setCreateddate(Date createddate) {
        this.createddate = createddate;
    }
}
