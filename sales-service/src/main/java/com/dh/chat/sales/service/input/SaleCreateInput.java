package com.dh.chat.sales.service.input;

import java.util.Date;

/**
 * @author Marvin Tola
 */
public class SaleCreateInput {

    private Long numbersale;

    private Date createdate;

    private Long employeid;

    private Long clientid;

    public Long getClientid() {
        return clientid;
    }

    public void setClientid(Long clientid) {
        this.clientid = clientid;
    }

    public Long getEmployeid() {
        return employeid;
    }

    public void setEmployeid(Long employeid) {
        this.employeid = employeid;
    }

    public Long getNumbersale() {
        return numbersale;
    }

    public void setNumbersale(Long numbersale) {
        this.numbersale = numbersale;
    }

    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }
}
