package com.dh.chat.sales.service.controller.api;

import com.dh.chat.sales.service.input.ClientCreateInput;
import com.dh.chat.sales.service.model.domain.Client;
import com.dh.chat.sales.service.service.ClientCreateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.annotation.RequestScope;

/**
 * @author MarvinTola
 */
@Api(tags = "client_controller",
        description = "Operation over Client"
)
@RestController
@RequestMapping("/public/clients")
@RequestScope
public class ClientController {

    @Autowired
    private ClientCreateService clientCreateService;

    @ApiOperation(
            value = "Create a client"
    )

    @RequestMapping(method = RequestMethod.POST)
    public Client createClient(@RequestBody ClientCreateInput input) {
        clientCreateService.setInput(input);
        clientCreateService.execute();

        return clientCreateService.getClient();
    }

}
