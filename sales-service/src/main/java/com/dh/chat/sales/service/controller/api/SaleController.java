package com.dh.chat.sales.service.controller.api;

import com.dh.chat.sales.service.input.SaleCreateInput;
import com.dh.chat.sales.service.model.domain.Sale;
import com.dh.chat.sales.service.service.SaleCreateService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Marvin Tola
 */
@RestController
@RequestMapping("/sale")
public class SaleController {

    @Autowired
    SaleCreateService saleCreateService;
    @ApiOperation(value = "end point to create sale")
    @ApiResponses({
            @ApiResponse(
                    code=401,
                    message = "Unauthorized te create sale"
            ),
            @ApiResponse(
                    code=404,
                    message = "Not found test"
            )
    })
    @RequestMapping(method = RequestMethod.POST)
    public Sale createSale(@RequestBody SaleCreateInput input){

        saleCreateService.setInput(input);
        saleCreateService.execute();
        return saleCreateService.getSale();

    }

}