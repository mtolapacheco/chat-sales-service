package com.dh.chat.sales.service.client.contact.service;

import com.dh.chat.contact.api.input.SystemContactCreateInput;
import com.dh.chat.sales.service.client.contact.model.Contact;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author Marvin Tola
 */
@FeignClient("${contact.service.name}")
interface ModuleContactClient {

    @RequestMapping(
            value = "/system/contacts",
            method = RequestMethod.POST
    )
    Contact createContact(@RequestBody SystemContactCreateInput input);
}