package com.dh.chat.sales.service.service;

import com.dh.chat.sales.service.framework.context.ServiceTransactional;
import com.dh.chat.sales.service.model.domain.Employee;
import com.dh.chat.sales.service.model.domain.TypeGender;
import com.dh.chat.sales.service.model.repositories.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * @author Marvin Tola
 */

@ServiceTransactional
public class EmployeeReadByGenderService {

    private TypeGender gender;

    private List<Employee> employees;

    @Autowired
    private EmployeeRepository employeeRepository;

    public  void execute(){
        employees=findEmployeeByGender(gender);
    }

    private List<Employee> findEmployeeByGender(TypeGender gender) {
        return employeeRepository.findByGender(gender);
    }

    public TypeGender getGender() {
        return gender;
    }

    public void setGender(TypeGender gender) {
        this.gender = gender;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public EmployeeRepository getEmployeeRepository() {
        return employeeRepository;
    }

    public void setEmployeeRepository(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }
}
