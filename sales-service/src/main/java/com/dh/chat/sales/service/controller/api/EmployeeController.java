package com.dh.chat.sales.service.controller.api;

import com.dh.chat.sales.service.input.EmployeeCreateInput;
import com.dh.chat.sales.service.model.domain.Employee;
import com.dh.chat.sales.service.model.domain.TypeGender;
import com.dh.chat.sales.service.service.EmployeeCreateService;
import com.dh.chat.sales.service.service.EmployeeReadByGenderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

import java.util.List;

/**
 * @author Marvin Tola
 */
@Api(tags = "employee",description = "Operation over employees")
@RestController
@RequestMapping("/public/employees")
@RequestScope
public class EmployeeController {

    @Autowired
    EmployeeCreateService employeeCreateService;


    @Autowired
    private EmployeeReadByGenderService employeeReadByGenderService;

    @ApiOperation(
            value = "create an employee"
    )
    @ApiResponses({
            @ApiResponse(
                    code=401,
                    message = "Unauthorized to create sale"
            ),
    })

    @RequestMapping(method = RequestMethod.POST)
    public Employee createEmployee(@RequestBody EmployeeCreateInput input){
        employeeCreateService.setInput(input);
        employeeCreateService.execute();
        return employeeCreateService.getEmployee();
    }

    @RequestMapping(value = "/list" ,method = RequestMethod.GET)
    public List<Employee> listEmployeeByTypeGender(@RequestParam("gender") TypeGender gender){
        employeeReadByGenderService.setGender(gender);
        employeeReadByGenderService.execute();

        return employeeReadByGenderService.getEmployees();

    }
}
