package com.dh.chat.sales.service.service;

import com.dh.chat.contact.api.input.SystemContactCreateInput;
import com.dh.chat.sales.service.client.contact.service.SystemContactService;
import com.dh.chat.sales.service.framework.context.ServiceTransactional;
import com.dh.chat.sales.service.input.EmployeeCreateInput;
import com.dh.chat.sales.service.model.domain.Employee;
import com.dh.chat.sales.service.model.repositories.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

/**
 * @author Marvin Tola
 */
//@Service
//@Scope("prototype")
@ServiceTransactional
public class EmployeeCreateService {

    private EmployeeCreateInput input;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private SystemContactService systemContactService;


    private Employee employee;

    public void execute(){
        Employee employeeInstance=composeEmployee();
        employee=employeeRepository.save(employeeInstance);

        createContact(employee);
    }

    private void createContact(Employee employee) {
        SystemContactCreateInput input =new SystemContactCreateInput();
        input.setAccountId(employee.getId());
        input.setUserId(employee.getId());
        input.setName(employee.getLastname()+" "+employee.getFirstname());
        input.setEmail(employee.getEmail());

        systemContactService.createContact(input);
    }

    private Employee composeEmployee() {
        Employee instance=new Employee();
        instance.setPosition(input.getPosition());
        instance.setFirstname(input.getFirstname());
        instance.setEmail(input.getEmail());
        instance.setLastname(input.getLastname());
        instance.setGender(input.getGender());
        instance.setIsdeleted(Boolean.TRUE);
        instance.setCreatedate(new Date());

        return instance;
    }

    public EmployeeCreateInput getInput() {
        return input;
    }

    public void setInput(EmployeeCreateInput input) {
        this.input = input;
    }

    public EmployeeRepository getEmployeeRepository() {
        return employeeRepository;
    }

    public void setEmployeeRepository(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
}
