package com.dh.chat.sales.service.client.contact.service;

import com.dh.chat.contact.api.input.SystemContactCreateInput;
import com.dh.chat.sales.service.client.contact.model.Contact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Marvin Tola
 */
@Service
public class SystemContactService {

    @Autowired
    private ModuleContactClient client;

    public Contact createContact(SystemContactCreateInput input) {
        return client.createContact(input);
    }
}
