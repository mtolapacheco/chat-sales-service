package com.dh.chat.sales.service.model.domain;

/**
 * @author Marvin Tola
 */
public enum TypeGender {
    MALE,FEMALE
}
