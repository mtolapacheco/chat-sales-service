package com.dh.chat.sales.service.input;

import com.dh.chat.sales.service.model.domain.TypeGender;

/**
 * @author Marvin Tola
 */
public class EmployeeCreateInput {

    private String position;

    private String firstname;

    private String lastname;

    private String email;

    private TypeGender gender;

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public TypeGender getGender() {
        return gender;
    }

    public void setGender(TypeGender gender) {
        this.gender = gender;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}