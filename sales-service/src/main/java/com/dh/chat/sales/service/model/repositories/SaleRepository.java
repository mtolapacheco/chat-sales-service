package com.dh.chat.sales.service.model.repositories;

import com.dh.chat.sales.service.model.domain.Sale;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author MarvinTola
 */
public interface SaleRepository extends JpaRepository<Sale,Long> {
}
