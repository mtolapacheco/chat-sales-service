package com.dh.chat.sales.service.model.repositories;

import com.dh.chat.sales.service.model.domain.Detail;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author MarvinTola
 */
public interface DetailRepository extends JpaRepository<Detail,Long> {
}
