package com.dh.chat.sales.service;


import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableFeignClients
@ComponentScan("com.dh.chat.sales.service")
@EnableAutoConfiguration
public class Config {
}
