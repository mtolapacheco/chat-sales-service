package com.dh.chat.sales.service.model.repositories;

import com.dh.chat.sales.service.model.domain.Employee;
import com.dh.chat.sales.service.model.domain.TypeGender;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * @author MarvinTola
 */
public interface EmployeeRepository extends JpaRepository<Employee,Long> {

    List<Employee> findAllByGender(TypeGender gender);

    @Query("select  item from Employee item where item.gender=:gender")
    List<Employee> findByGender(@Param("gender")TypeGender gender);
}
