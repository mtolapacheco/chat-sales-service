package com.dh.chat.sales.service.service;

import com.dh.chat.sales.service.input.SaleCreateInput;
import com.dh.chat.sales.service.model.domain.Client;
import com.dh.chat.sales.service.model.domain.Employee;
import com.dh.chat.sales.service.model.domain.Sale;
import com.dh.chat.sales.service.model.repositories.ClientRepository;
import com.dh.chat.sales.service.model.repositories.EmployeeRepository;
import com.dh.chat.sales.service.model.repositories.SaleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @author Marvin Tola
 */
@Scope("prototype")
@Service
public class SaleCreateService {

    private SaleCreateInput input;

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private SaleRepository saleRepository;


    private Sale sale;

    public void execute(){

        Employee employee = getEmployeInstance();

        Client client =getClientInstance();

        Sale saleInstance =composeSaleInstance(employee,client);
        sale=saleRepository.save(saleInstance);

    }

    private Client getClientInstance() {
        return clientRepository.findById(input.getClientid()).get();
        //retornamos un id de un cliente de la base de datos
    }

    public Employee getEmployeInstance(){
        return employeeRepository.findById(input.getEmployeid()).get();
        //retornamos un empleado de la base de datos
    }

    private Sale composeSaleInstance(Employee employee, Client client) {

        Sale instance =new Sale();
        instance.setNumbersale(input.getNumbersale());
        instance.setCreateddate(new Date());
        instance.setEmployee(employee);
        instance.setClient(client);
        return  instance;
    }


    public Sale getSale() {
        return sale;
    }

    public void setSale(Sale sale) {
        this.sale = sale;
    }

    public SaleCreateInput getInput() {
        return input;
    }

    public void setInput(SaleCreateInput input) {
        this.input = input;
    }

    public ClientRepository getClientRepository() {
        return clientRepository;
    }

    public void setClientRepository(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    public EmployeeRepository getEmployeeRepository() {
        return employeeRepository;
    }

    public void setEmployeeRepository(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public SaleRepository getSaleRepository() {
        return saleRepository;
    }

    public void setSaleRepository(SaleRepository saleRepository) {
        this.saleRepository = saleRepository;
    }
}
